# Terminal 1 React Assessment
---
## Development
To start development, please install the node packages and run the dev environment
```
npm install
npm start
```
<br>

## Production
Run the following command to production files
```
npm run build
```
<br>

## Preview Link
```
https://t1-react.imshing.com/
```

<br>

## Git Repo
```
https://gitlab.com/hclojacky/terminal-1-react-assessment
```