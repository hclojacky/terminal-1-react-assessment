import React from 'react'
import { connect } from 'react-redux'
import { calculateTotalPrice, addQuantity, removeQuantity, removeItem } from '../../stores/actions/index'

import './cartList.scss'
import CartRow from './cartRow/cartRow'

class CartList extends React.Component {
  componentDidMount() {
    this.props.calculateTotalPrice()
  }

  removeItemHandler = (id) => {
    const promise = new Promise((res, rej) => {
      this.props.removeItem(id)
      res()
    })
    promise.then(() => {
      this.props.calculateTotalPrice()
    })
  }

  incrementHandler = (id) => {
    const promise = new Promise((res, rej) => {
      this.props.addQuantity(id)
      res()
    })
    promise.then(() => {
        this.props.calculateTotalPrice()
      })
  }

  reductionHandler = (id) => {
    const promise = new Promise((res, rej) => {
      this.props.removeQuantity(id)
      res()
    })
    promise.then(() => {
        this.props.calculateTotalPrice()
      })
  }

  render() {
    return (
      <div className="cart-list">
        <div className="cart-list__container">
          <div className="cart-list__title">{this.props.title}</div>
          <div className="cart-list__listing">
            {
              this.props.orders.map((item) => {
                return (
                  <div className="cart-list__listing-item" key={item.id}>
                    <CartRow {...item} incrementFunc={this.incrementHandler} reductionFunc={this.reductionHandler} removeItemFunc={this.removeItemHandler} updateQuantityFunc={this.updateQuantityHandler} />
                  </div>
                )
              })
            }
          </div>
          <div className="cart-list__row">
            <div className="cart-list__back">
              <div className="icon-arrow"></div>
              <div className="cart-list__back-text">Continue Shopping</div>
            </div>
            <div className="cart-list__subtotal">
              <div className="cart-list__subtotal-label">Subtotal:</div>
              <div className="cart-list__subtotal-num">${this.props.totalPrice}</div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

CartList.defaultProps = {
  title: 'Shopping Cart',
}

const mapStateToProps = state => {
  console.log(state)
  return {
    orders: state.cartList.orders,
    totalPrice: state.cartList.totalPrice
  }
}

export default connect(mapStateToProps, { calculateTotalPrice, addQuantity, removeQuantity, removeItem })(CartList)
