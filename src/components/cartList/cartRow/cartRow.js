import React from 'react'
import './cartRow.scss'
import NumberFormat from 'react-number-format'

const CartRow = (props) => {
  return (
    <div className="cart-row">
      <div className="cart-row__container">
        <div className="cart-row__thumbnail">
          <img src={props.thumbnail} alt={props.name} />
        </div>
        <div className="cart-row__info">
          <div className="cart-row__info-name">{props.name}</div>
          <div className="cart-row__info-id">#{props.id}</div>
        </div>
        <div className="cart-row__quantity">
          <div className={`cart-row__quantity-icon ${props.quantity === 0 ? 'disabled' : null}`} onClick={() => props.reductionFunc(props.id)}>-</div>
          <div className="cart-row__quantity-num">{props.quantity}</div>
          <div className="cart-row__quantity-icon" onClick={() => props.incrementFunc(props.id)}>+</div>
        </div>
        <div className="cart-row__price">
          <div className="cart-row__price-text">
            <NumberFormat value={`${props.price * props.quantity}`} displayType={'text'} decimalScale={2} fixedDecimalScale={true}/>
          </div>
        </div>
        <div className="cart-row__delete">
          <div className="icon-close" onClick={() => props.removeItemFunc(props.id)}></div>
        </div>
      </div>
    </div>
  )
}

export default CartRow
