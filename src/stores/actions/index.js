import { CALCULATE_TOTAL_PRICE, ADD_QUANTITY, REMOVE_QUANTITY, REMOVE_ITEM } from './actionTypes'

export const calculateTotalPrice = () => ({
  type: CALCULATE_TOTAL_PRICE,
});

export const addQuantity = (id) => ({
  type: ADD_QUANTITY,
  payload: {
    id
  }
})

export const removeQuantity = (id) => ({
  type: REMOVE_QUANTITY,
  payload: {
    id
  }
})

export const removeItem = (id) => ({
  type: REMOVE_ITEM,
  payload: {
    id
  }
})