import { CALCULATE_TOTAL_PRICE, ADD_QUANTITY, REMOVE_ITEM, REMOVE_QUANTITY } from '../actions/actionTypes'

const initialState = {
  orders: [
    {
      id: 4231648,
      thumbnail: 'https://source.unsplash.com/500x500/?chicken,food',
      name: 'Chicken momo',
      price: 10.5,
      quantity: 1
    },
    {
      id: 4231620,
      thumbnail: 'https://source.unsplash.com/500x500/?potatoes,food',
      name: 'Spicy Mexican potatoes',
      price: 8.5,
      quantity: 1
    },
    {
      id: 4231333,
      thumbnail: 'https://source.unsplash.com/500x500/?breakfast,food',
      name: 'All day Breakfast',
      price: 5.9,
      quantity: 1
    }
  ],
  totalPrice: 0
}

export default function(state = initialState, action) {
  switch(action.type) {
    case CALCULATE_TOTAL_PRICE: {
      let totalPrice = 0
      state.orders.forEach(order => {
        totalPrice += order.quantity * order.price
      })
      return {
        ...state,
        totalPrice: totalPrice
      }
    }
    case ADD_QUANTITY: {
      const { id } = action.payload
      const { orders } = state
      const newOrders = orders.reduce((acc, cur) => {
        if (cur.id === id) {
          cur.quantity++
        }
        acc.push(cur)
        return acc
      }, [])
      return {
        ...state,
        orders: newOrders
      }
    }
    case REMOVE_QUANTITY: {
      const { id } = action.payload
      const { orders } = state
      const newOrders = orders.reduce((acc, cur) => {
        if (cur.id === id) {
          cur.quantity--
        }
        acc.push(cur)
        return acc
      }, [])
      return {
        ...state,
        orders: newOrders
      }
    }
    case REMOVE_ITEM: {
      const { id } = action.payload
      const { orders } = state
      const newOrders = orders.filter(item => {
        return item.id !== id
      })
      return {
        ...state,
        orders: newOrders
      }
    }
    default:
      return state;
  }
}